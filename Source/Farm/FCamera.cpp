// Fill out your copyright notice in the Description page of Project Settings.

#include "FCamera.h"
#include "Camera/CameraComponent.h"
#include "Camera/PlayerCameraManager.h"
#include "GameFramework/SpringArmComponent.h"



// Sets default values
AFCamera::AFCamera()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));

	RootComponent = SpringArm;
	Camera->SetupAttachment(SpringArm);


	MinZoomInDistance = 500.0f;

	MaxZoomOutDistance = 3500.0f;


}

// Called when the game starts or when spawned
void AFCamera::BeginPlay()
{
	Super::BeginPlay();
	
	APlayerController* PC = GetWorld()->GetFirstPlayerController();
	FViewTargetTransitionParams FT;
	PC->SetViewTarget(this, FT);
}

// Called every frame
void AFCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

