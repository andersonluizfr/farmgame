// Fill out your copyright notice in the Description page of Project Settings.

#include "FEntityPreview.h"


AFEntityPreview::AFEntityPreview() 
{
	MeshPreview = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Preview Mesh"));

	RootComponent = MeshPreview;
}

