// Fill out your copyright notice in the Description page of Project Settings.

#include "FPlayerController.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Engine.h"
#include "Engine/DataTable.h"
#include "Widgets/FWInventory.h"
#include "Kismet/GameplayStatics.h"
#include "Blueprint/UserWidget.h"


AFPlayerController::AFPlayerController() 
{
	bShowMouseCursor = true;
	PrimaryActorTick.bCanEverTick = true;

	

	static ConstructorHelpers::FObjectFinder<UDataTable> 
		GameObjectLookupDataTable_BP(TEXT("DataTable'/Game/Blueprints/DT_Blueprints.DT_Blueprints'"));
	
	DataBlueprints = GameObjectLookupDataTable_BP.Object;
	



	



}


void AFPlayerController::BeginPlay()
{
	Super::BeginPlay();

	MakeInventory();

	//Plot();

	TArray<AActor*> Result;
	UGameplayStatics::GetAllActorsOfClass(this, AFCamera::StaticClass(), Result);

	CameraGame = Cast<AFCamera>(Result[0]);
	


	if (_wPlantation) 
	{
		PlantationWidget = Cast<UFWPlantationInfo>(CreateWidget<UUserWidget>(this, _wPlantation));
		PlantationWidget->AddToViewport();
		PlantationWidget->SetVisibility(ESlateVisibility::Hidden);
	}
}

void AFPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	this->InputComponent->BindAction("Click", EInputEvent::IE_Pressed, this, &AFPlayerController::OnClick);
	this->InputComponent->BindAction("Open Inventory", EInputEvent::IE_Pressed, this, &AFPlayerController::OpenInventory);
	this->InputComponent->BindAction("ZoomIn", EInputEvent::IE_Pressed,this, &AFPlayerController::ZoomIn);
	this->InputComponent->BindAction("ZoomOut", EInputEvent::IE_Pressed, this, &AFPlayerController::ZoomOut);
	
	this->InputComponent->BindAxis("Move Forward", this, &AFPlayerController::MoveForward);
	this->InputComponent->BindAxis("Move Left", this, &AFPlayerController::MoveLeft);
	

}

void AFPlayerController::MakeInventory()
{
	Inventory.Materials.Add(FMaterials(0, "Wood", "More Wood", NULL),500);
	Inventory.Materials.Add(FMaterials(1, "Iron", "More Iron",NULL),600);
	Inventory.InventorySize = 16;

	

	static const FString ContextString(TEXT("GENERAL"));
	FName RowName = FName(TEXT("0"));
	
	FBlueprints* Row = DataBlueprints->FindRow<FBlueprints>(RowName, ContextString);

	if (Row) {
		UE_LOG(LogTemp, Warning, TEXT("%s"), *Row->Description)
			//FBlueprints blueprint(*Row);
			int32 i = Inventory.Blueprints.Add(*Row);
		UE_LOG(LogTemp, Warning, TEXT("index %d"), i)
			//Inventory.Materials.Add(FMaterials(1, "Iron", "More Iron"), 600);
	}
	
}

void AFPlayerController::Tick(float DeltaTime)
{
	if (bPloting) 
	{
		FHitResult Hit;
		GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery1, true, Hit);
		if (Hit.bBlockingHit) {
			FVector Location = Hit.Location;
			ActiveEntity->SetActorLocation(Location);
		}
	}
	else {
		/*if(*Inventory.Blueprints[0].Name != NULL)
			UE_LOG(LogTemp, Warning, TEXT("%s"), *Inventory.Blueprints[0].Description)*/

	}
}

void AFPlayerController::Plot()
{
	ActiveBlueprint = 0; 
	FBlueprints Bp  = Inventory.Blueprints[ActiveBlueprint];

	FHitResult Hit;
	GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery1, true, Hit); 
	
	/*if (Hit.bBlockingHit) {
		FVector Position = Hit.Location;
		FRotator Rotation = FRotator::ZeroRotator;
		ActiveEntiry = Cast<AFEntity>(GetWorld()->SpawnActor(Bp.ObjectPreview, &Position, &Rotation));
		if(ActiveEntiry)
			bPloting = true;
	}*/
	FVector Position = FVector(0,0,0);
	FRotator Rotation = FRotator::ZeroRotator;
	ActiveEntity = Cast<AFEntity>(GetWorld()->SpawnActor(Bp.ObjectPreview, &Position, &Rotation));
	if (ActiveEntity)
		bPloting = true;


}

void AFPlayerController::OnClick()
{
	//traces for visibility (change later fo a custom channel)
	FHitResult Hit;
	GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery1, true, Hit);

	

	if (Hit.bBlockingHit) 
	{
		FVector Position = Hit.Location;
		FRotator Rotation = FRotator::ZeroRotator;
		//Debug
		UE_LOG(LogTemp, Warning, TEXT("%s"), *Hit.Actor.Get()->GetName())
		
			/* If we a ploting a entity we spawn the prebuilding at location*/
		if (bPloting) {
			FBlueprints Bp = Inventory.Blueprints[ActiveBlueprint];
			GetWorld()->SpawnActor(Bp.Object, &Position, &Rotation);
			ActiveEntity->Destroy();
			Inventory.Blueprints.RemoveAt(ActiveBlueprint);

			bPloting = false;
			return;
		}

		APlantation* Plantation = Cast<APlantation>(Hit.GetActor());
		AFPreBuilding* PreBuilding = Cast<AFPreBuilding>(Hit.GetActor());

		if (Plantation != nullptr) {
			ActivePlantation = Plantation;
			PlantationWidget->Generate(_wSlot,Plantation->Size, Plantation->Slots);
			
			this->SetInputMode(FInputModeGameAndUI());
			
			PlantationWidget->SetVisibility(ESlateVisibility::Visible);
		}
		else if(PreBuilding != nullptr) {

			if (PlantationWidget->IsVisible()) {
				PlantationWidget->SetVisibility(ESlateVisibility::Hidden);
			}
			
			if (PreBuilding->HasAllMaterials(Inventory.Materials)) {
				UE_LOG(LogTemp,Warning, TEXT("Has All"))
			}

		}
		else {
			if (PlantationWidget->IsVisible()) {
				PlantationWidget->SetVisibility(ESlateVisibility::Hidden);
			}
		
		}



	}


}

void AFPlayerController::OpenInventory()
{
	if (_wInventory) {


		if (InventoryWidget == nullptr) {

			InventoryWidget = CreateWidget<UFWInventory>(this, _wInventory);
			InventoryWidget->AddToViewport();
			InventoryWidget->GenerateInventory();
			return;
		}

		if (InventoryWidget != nullptr) {
		
			if (InventoryWidget->Visibility == ESlateVisibility::Hidden ) {
				
				InventoryWidget->SetVisibility(ESlateVisibility::Visible);
				InventoryWidget->GenerateInventory();
			}
			else if (InventoryWidget->Visibility == ESlateVisibility::Visible ||
				InventoryWidget->Visibility == ESlateVisibility::SelfHitTestInvisible) {
				
				InventoryWidget->SetVisibility(ESlateVisibility::Hidden);
			}
		
		}


		


	}
}

void AFPlayerController::MoveForward(float Value)
{
	
	FVector Delta = FVector(1.0, 0, 0);
	float Speed = 1000.0f;
	Delta = Delta * Speed;

	

	if(CameraGame != nullptr)
		CameraGame->AddActorWorldOffset(Delta*Value *GetWorld()->DeltaTimeSeconds);
}

void AFPlayerController::MoveLeft(float Value)
{
	FVector Delta = FVector(0, 1.0, 0);
	float Speed = 1000.0f;
	Delta = Delta * Speed;
	if (CameraGame != nullptr)
		CameraGame->AddActorWorldOffset(Delta*Value *GetWorld()->DeltaTimeSeconds);
}

void AFPlayerController::ZoomIn()
{

	float CameraPositionZ = CameraGame->GetActorLocation().Z;
	float Max = CameraGame->MaxZoomOutDistance;
	float Min = CameraGame->MinZoomInDistance;

	float Speed = 2500.0f;
	FVector Delta = FVector(0,0, -1*Speed * GetWorld()->DeltaTimeSeconds);
	
	if (Delta.Z + CameraPositionZ  > Min) {
		if (CameraGame != nullptr)
			CameraGame->AddActorWorldOffset(Delta * Speed * GetWorld()->DeltaTimeSeconds);

	}
	else {
		if (CameraGame != nullptr)
			CameraGame->SetActorLocation(FVector(CameraGame->GetActorLocation().X, CameraGame->GetActorLocation().Y, Min));
	}
	UE_LOG(LogTemp, Warning, TEXT("%f"), CameraGame->GetActorLocation().Z)

	
}

void AFPlayerController::ZoomOut()
{
	UE_LOG(LogTemp, Warning, TEXT("%f"), CameraGame->GetActorLocation().Z)
	float CameraPositionZ = CameraGame->GetActorLocation().Z;
	float Max = CameraGame->MaxZoomOutDistance;
	float Min = CameraGame->MinZoomInDistance;

	float Speed = 2500.0f;
	FVector Delta = FVector(0, 0, 1 * Speed * GetWorld()->DeltaTimeSeconds);

	if (Delta.Z + CameraPositionZ < Max) {
		if (CameraGame != nullptr)
			CameraGame->AddActorWorldOffset(Delta * Speed * GetWorld()->DeltaTimeSeconds );

	}
	else {
		if (CameraGame != nullptr)
			CameraGame->SetActorLocation(FVector(CameraGame->GetActorLocation().X, CameraGame->GetActorLocation().Y, Max));
	}

	
}

