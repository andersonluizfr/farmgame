// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/DataTable.h"
#include "FEntity.generated.h"






UCLASS()
class FARM_API AFEntity : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFEntity();

	
};



USTRUCT(Blueprintable)
struct FBlueprints : public FTableRowBase {
	GENERATED_BODY()

public:
	FBlueprints() {}
	

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Blueprints")
		FString Name;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Blueprints")
		FString Description;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Blueprints")
		int32 Price;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Blueprints")
		TSubclassOf<AFEntity> Object;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Blueprints")
		TSubclassOf<AFEntity> ObjectPreview;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Blueprints")
		UTexture2D* BlueprintThumbnail;




};