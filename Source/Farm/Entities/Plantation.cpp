// Fill out your copyright notice in the Description page of Project Settings.

#include "Plantation.h"
#include "Seeds/FPotatoSeed.h"


// Sets default values
APlantation::APlantation()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	FenceSM = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Fence Static Mesh"));
	RootComponent = FenceSM;

	Timer = 1.0f;
	Size = 9;
	Slots.SetNum(Size);
}

// Called when the game starts or when spawned
void APlantation::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlantation::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Timer <= 0) 
	{
		Timer = 1.0f;
		
		for (int32 index = 0;  index< Slots.Num(); index++) 
		{
			if (Slots[index]._seed != nullptr) 
			{
				if (Slots[index]._seed->GetSeedState() != ESeedState::Rotten) 
				{
					FString SeedName = Slots[index]._seed->SeedName;
					FString SeedState = Slots[index]._seed->StateName;
					//Debug
					UE_LOG(LogTemp, Warning, TEXT("%s "), *SeedName)
					UE_LOG(LogTemp, Warning, TEXT("%s "), *SeedState)
					//Decreases 1 second from seed's time to change
					Slots[index]._seed->ActualTime -= 1.0;
					if (Slots[index]._seed->ActualTime <= 0.0f) {
						Slots[index]._seed->NextState();
						Slots[index]._seed->ActualTime = Slots[index]._seed->TimetoChange;
						Slots[index]._seed->SeedIndex = index;
					}
			
				}
			
			}
			
		}
		/*if (Removables.Num() > 0) 
		{
			for (int32& index : Removables) 
			{
				Slots.RemoveAt(index);
			}
		}

		Removables.Empty();*/
	
	}
	
	Timer -= DeltaTime;

}

void APlantation::AddSeed(const TSubclassOf<AFSeed>& _newSeed, int32 _slotIndex)
{
	if (Seeds < Size) 
	{
		FSlot _newSlot;
		 
		//Gets SocketName from slot index.
		FName SocketName =FName(* FString::FromInt(_slotIndex));

		FVector Location = FenceSM->GetSocketLocation(SocketName);
		FRotator Rotation = FRotator::ZeroRotator;
		
		_newSlot._seed = Cast<AFSeed>(GetWorld()->SpawnActor(_newSeed, &Location, &Rotation));
		_newSlot._seed->SetOwner(this);

		//Slots.Add(_newSlot);
		//Slots.Insert(_newSlot, _slotIndex);
		Slots[_slotIndex] = _newSlot;
		Seeds++;
	}
}

