// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/Actor.h"
#include "Engine/DataTable.h"
#include "Plantation.h"
#include "FEntity.h"
#include "FPreBuilding.generated.h"





USTRUCT(Blueprintable)
struct FMaterials : public FTableRowBase
{
	GENERATED_BODY()
public:
	FMaterials() {}
	FMaterials(int32 _MaterialID, FString _MaterialName, FString _MaterialDescription, UTexture2D* _Thumb)
		: MaterialID(_MaterialID), MaterialName(_MaterialName), MaterialDescription(_MaterialDescription), ThumbNail(_Thumb) {}

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Crafting")
	FString MaterialName;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Crafting")
	int32 MaterialID;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Crafting")
	FString MaterialDescription;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Crafting")
	UTexture2D* ThumbNail;

	bool operator== (const FMaterials& Other)  const {
		return this->MaterialName.Equals(Other.MaterialName,ESearchCase::Type::IgnoreCase);
	}

	friend int32 GetTypeHash(const FMaterials& Other)
	{
		return GetTypeHash(Other.MaterialID);
	}

};

USTRUCT(Blueprintable)
struct FRecipe : public FTableRowBase{

	GENERATED_BODY()
public:


	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Recipe")
	FString RecipeName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Recipe")
	int32 RecipeID;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Recipe")
	TMap<FMaterials, int32> MaterialsAmount;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Recipe")
	UTexture2D* ThumbNail;

};



UCLASS()
class FARM_API AFPreBuilding : public AFEntity
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFPreBuilding();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* PreSM;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Recipe")
	FRecipe Recipe;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Plantation")
	TSubclassOf<APlantation> cPlantation;
	
	bool HasAllMaterials(TMap<FMaterials, int32> & PlayerMaterials);
	
};
