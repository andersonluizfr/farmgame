// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FEntity.h"
#include "FEntityPreview.generated.h"

/**
 * 
 */
UCLASS()
class FARM_API AFEntityPreview : public AFEntity
{
	GENERATED_BODY()
	
public:
	AFEntityPreview();
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite,Category = "Mesh")
	UStaticMeshComponent* MeshPreview;
	
	
};
