// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "FCamera.h"
#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Widgets/FWPlantationInfo.h"
#include "Plantation.h"
#include "FPreBuilding.h"
#include "Widgets/FWInventorySlot.h"
//#include "FWInventory.h"
#include "FPlayerController.generated.h"

class UDataTable;


/**
 * 
 */
class UFWInventory;

USTRUCT(Blueprintable)
struct FInventory {


	GENERATED_BODY()

public:

	UPROPERTY()
	int32 InventorySize;

	UPROPERTY()
	int32 TotalItems;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Inventory")
	TMap<FString, int32> Seeds;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Inventory")
	TMap<FMaterials, int32> Materials;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Inventory")
	TArray<FBlueprints> Blueprints;

	UPROPERTY()
	int32 Golds;

};

UCLASS()
class FARM_API AFPlayerController : public APlayerController
{
	GENERATED_BODY()

public:

		AFPlayerController();
		//widget Class -----------------------------------------------
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<UUserWidget> _wPlantation;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<UUserWidget> _wSlot;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<UUserWidget> _wSlotInfo;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<UUserWidget> _wInventorySlot;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<UUserWidget> _wInventory;


		//widget Variables
		UPROPERTY()
		UFWPlantationInfo* PlantationWidget;
		
		UPROPERTY()
		UFWInventory* InventoryWidget;
		
		UPROPERTY(EditDefaultsOnly,BlueprintReadWrite, Category = "Inventory")
		FInventory Inventory;
		
		UPROPERTY()
		APlantation* ActivePlantation;

		bool bPloting;

		//Camera
		UPROPERTY()
		AFCamera* CameraGame;

		/*Input functions*/
		UFUNCTION()
		void OnClick();
		UFUNCTION()
		void OpenInventory();
		UFUNCTION()
		void MoveForward(float Value);
		UFUNCTION()
		void MoveLeft(float Value);

		UFUNCTION()
		void ZoomIn();

		UFUNCTION()
		void ZoomOut();

	

		//database
		UPROPERTY()
		UDataTable* DataBlueprints;
	
protected:
	virtual void BeginPlay() override;
		
	virtual void SetupInputComponent() override;

	void MakeInventory();

public:

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void Plot();

private:
	UPROPERTY()
	AFEntity* ActiveEntity;

	int32 ActiveBlueprint;
	
};
