// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Seeds/Seed.h"
#include "Seeds/FSeed.h"
#include "FEntity.h"
#include "Plantation.generated.h"

USTRUCT(BlueprintType)
struct FSlot
{
	GENERATED_BODY()

	UPROPERTY()
	AFSeed* _seed = NULL;

};

UCLASS()
class FARM_API APlantation : public AFEntity
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlantation();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* FenceSM;

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	int32 Size;
	int32 Seeds;

	TArray<FSlot> Slots;
	TArray<int32> Removables;
	
	float Timer;
	UFUNCTION(BlueprintCallable, Category = "Farm")
	void AddSeed(const TSubclassOf<AFSeed>& _newSeed, int32 _slotIndex);
	
};
