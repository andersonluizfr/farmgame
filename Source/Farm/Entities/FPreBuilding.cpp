// Fill out your copyright notice in the Description page of Project Settings.

#include "FPreBuilding.h"


// Sets default values
AFPreBuilding::AFPreBuilding()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	PreSM = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	RootComponent = PreSM;

}

// Called when the game starts or when spawned
void AFPreBuilding::BeginPlay()
{
	Super::BeginPlay();
	
}

bool AFPreBuilding::HasAllMaterials(TMap<FMaterials, int32> & PlayerMaterials)
{
	bool bHasAll = false;
	int32 materials = Recipe.MaterialsAmount.Num();
	TArray<bool> CheckAll;
	CheckAll.SetNum(materials);
	int32 index = 0;
	for (auto it = Recipe.MaterialsAmount.CreateConstIterator(); it; ++it) {
		for (auto p = PlayerMaterials.CreateConstIterator(); p; ++p) {
			if (it->Key == p->Key) {
				if (it->Value <= p->Value) {
					CheckAll[index] = true;
					index++;
				}
			}
		}

	}

	return CheckAll.Contains(false) ? false : true;


}



