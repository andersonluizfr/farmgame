// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FWInventory.generated.h"

/**
 * 
 */
UCLASS()
class FARM_API UFWInventory : public UUserWidget
{
	GENERATED_BODY()
	
public:

	void GenerateInventory();
	
protected:
	virtual void NativeConstruct() override;
	
	
};
