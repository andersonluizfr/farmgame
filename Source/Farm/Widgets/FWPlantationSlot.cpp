// Fill out your copyright notice in the Description page of Project Settings.

#include "FWPlantationSlot.h"


void UFWPlantationSlot::NativeConstruct() {
	
	Super::NativeConstruct();
	if(SlotButton == nullptr)
		SlotButton = Cast<UButton>(GetWidgetFromName("B_Slot"));

	if (!SlotButton->OnClicked.IsBound()) {
		
		SlotButton->OnClicked.AddDynamic(this, &UFWPlantationSlot::OnSlotClicked);
	
	}

}

void UFWPlantationSlot::OnSlotClicked()
{
	//OnClickedSlot.Broadcast(index);
	OnClickedSlot.Execute(index);
	
}
