// Fill out your copyright notice in the Description page of Project Settings.


#include "FWInventory.h"
#include "Entities/FPlayerController.h"




void UFWInventory::NativeConstruct()
{

	UWrapBox* Box = Cast<UWrapBox>(GetWidgetFromName("WBox"));
	
	AFPlayerController* PC = Cast<AFPlayerController>(GetOwningPlayer());
	
	for (int i = 0; i < PC->Inventory.InventorySize; i++) {
		UFWInventorySlot* InvSlot = CreateWidget< UFWInventorySlot>(PC, PC->_wInventorySlot);
		Box->AddChild(InvSlot);

	}

}


void UFWInventory::GenerateInventory() {

	UWrapBox* Box = Cast<UWrapBox>(GetWidgetFromName("WBox"));
	int32 index = 0;

	AFPlayerController* PC = Cast<AFPlayerController>(GetOwningPlayer());

	if (Box && Box->HasAnyChildren()) {

		for (FBlueprints &p : PC->Inventory.Blueprints) {
			UFWInventorySlot* InvSlot = Cast<UFWInventorySlot>(Box->GetChildAt(index));
			InvSlot->SetImage(p.BlueprintThumbnail);
		}

	}


}

