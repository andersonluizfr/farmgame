// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/Components/Button.h"
#include "FWPlantationSlot.generated.h"


DECLARE_DYNAMIC_DELEGATE_OneParam(FOnClickedButtonDelegate, int32, SlotIndex);
/**
 * 
 */
UCLASS()
class FARM_API UFWPlantationSlot : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	virtual void NativeConstruct() override;
public:

	//UFWPlantationSlot(const FObjectInitializer& ObjectInitializer);

	

	UFUNCTION(BlueprintImplementableEvent, Category = "Status")
	void SlotStatus(bool _bEmpty);

	/*UFUNCTION()
	void UpdateInfo();
	*/
	FOnClickedButtonDelegate OnClickedSlot;

	int32 index;

	UButton* SlotButton;
	
	UFUNCTION()
	void OnSlotClicked();

};
