// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/Components/Button.h"
#include "FWPlantationInfoSlot.generated.h"

/**
 * 
 */
UCLASS()
class FARM_API UFWPlantationInfoSlot : public UUserWidget
{
	GENERATED_BODY()
	


public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Widget")
	FString SeedStatus;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Widget")
	FString SeedName;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Widget")
	float Timer;

	int32 ActivateButton;

	UFUNCTION(BlueprintImplementableEvent,Category = "Variables" )
	void UpdateSeedName();
	
	UFUNCTION(BlueprintImplementableEvent, Category = "Variables")
	void UpdateSeedStatus();
	
	UFUNCTION(BlueprintImplementableEvent, Category = "Variables")
	void UpdateSeedTimer();

	
	UFUNCTION()
	void CallUpdateSeedState(FString _newState, int32 _index);
	
};
