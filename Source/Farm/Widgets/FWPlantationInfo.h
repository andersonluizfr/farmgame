// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FWPlantationInfoSlot.h"
#include "FWPlantationSlot.h"
#include "Entities/Plantation.h"
#include "Runtime/UMG/Public/Components/WrapBox.h"
#include "Runtime/UMG/Public/Components/VerticalBox.h"
#include "Runtime/UMG/Public/Components/Button.h"
#include "FWPlantationInfo.generated.h"


/**
 * 
 */
UCLASS()
class FARM_API UFWPlantationInfo : public UUserWidget
{
	GENERATED_BODY()

protected:

	virtual void NativeConstruct() override;

public:

	

	void Generate(TSubclassOf<UUserWidget>& _wSlot,int32 slots,TArray<FSlot>& _slots);
	
	UFUNCTION()
	void ShowSlotInfo(int32 SlotIndex);

	UFUNCTION()
	void UpdateSlotState(FString State, int32 Index);

private:
	
	UFWPlantationInfoSlot* SlotInfo;
	UWrapBox* Box;
	UVerticalBox* VBox;
};
