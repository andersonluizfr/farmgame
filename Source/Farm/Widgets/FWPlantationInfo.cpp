// Fill out your copyright notice in the Description page of Project Settings.

#include "FWPlantationInfo.h"
#include "Entities/FPlayerController.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"


void UFWPlantationInfo::NativeConstruct()
{
	Super::NativeConstruct();
	UE_LOG(LogTemp, Warning, TEXT("Genarated"))
	/* creates slots and theirs informations*/

	if(Box  == nullptr || VBox == nullptr || SlotInfo == nullptr)
	{
		Box = Cast<UWrapBox>(GetWidgetFromName("Box"));
		/*Gets the vertical box with the slotinfo widget*/
		VBox = Cast<UVerticalBox>(GetWidgetFromName("V_Box"));
	
		SlotInfo = Cast<UFWPlantationInfoSlot>(VBox->GetChildAt(0));
	}
}


void UFWPlantationInfo::Generate(TSubclassOf<UUserWidget>& _wSlot,int32 slots, TArray<FSlot>& _slots)
{
	if (_wSlot) 
	{
		
		
		//remove slots
		Box->ClearChildren();
		
		//Debug
		if (GEngine) 
		{
			GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Blue, FString::FromInt(Box->GetChildrenCount()) );
		}
		

		if (SlotInfo) 
		{
			for (int32 i = 0; i < slots; i++) {
				UFWPlantationSlot* slot = Cast<UFWPlantationSlot>(CreateWidget<UUserWidget>(GetOwningPlayer(), _wSlot));
				if (slot) {
					if (_slots[i]._seed != nullptr)
						slot->SlotStatus(false);	
					else
						slot->SlotStatus(true);

					slot->index = i;
					Box->AddChild(slot);
					/*Binds onpressed function*/
					//UButton* SlotButton = Cast<UButton>(slot->GetWidgetFromName("B_Slot"));
					slot->OnClickedSlot.BindUFunction(this, FName("ShowSlotInfo"));
					
				}

			
			}
		
		}

	}

}

void UFWPlantationInfo::ShowSlotInfo(int32 SlotIndex)
{
	
	UE_LOG(LogTemp, Warning, TEXT("Clicked %d"), SlotIndex)

	AFPlayerController* PC = Cast<AFPlayerController>(GetOwningPlayer());

	if (PC) {
		//set SlotInfo info
		/*Checks if slot is filled and then set its info*/
		if (PC->ActivePlantation->Slots[SlotIndex]._seed != nullptr) {
			if(!PC->ActivePlantation->Slots[SlotIndex]._seed->StateChanged.IsBound())
				PC->ActivePlantation->Slots[SlotIndex]._seed->StateChanged.AddDynamic(this, &UFWPlantationInfo::UpdateSlotState);
			
			SlotInfo->SeedName = PC->ActivePlantation->Slots[SlotIndex]._seed->SeedName;
			SlotInfo->SeedStatus = PC->ActivePlantation->Slots[SlotIndex]._seed->StateName;
			SlotInfo->UpdateSeedName();
			SlotInfo->UpdateSeedStatus();
			SlotInfo->ActivateButton = SlotIndex;

		}
		else {
			/*Set Default values for empty slots*/
			SlotInfo->SeedName = "None";
			SlotInfo->SeedStatus = "None";
			SlotInfo->UpdateSeedName();
			SlotInfo->UpdateSeedStatus();
			SlotInfo->ActivateButton = SlotIndex;
		
		}

	}



}

void UFWPlantationInfo::UpdateSlotState(FString State, int32 Index)
{
	if (SlotInfo->ActivateButton == Index) 
	{
		AFPlayerController* PC = Cast<AFPlayerController>(GetOwningPlayer());

		if (PC) {

			if (PC->ActivePlantation->Slots[Index]._seed != nullptr) {
				SlotInfo->SeedStatus = PC->ActivePlantation->Slots[Index]._seed->StateName;
				SlotInfo->UpdateSeedStatus();
			}
	
		}
	
	}


}
