// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "FSeed.h"
#include "CoreMinimal.h"

/**
 * 
 */

class FARM_API Seed
{
public:
	Seed();
	~Seed();

	float ActualTime;
	float TimetoChange;
	FString SeedName;
	FString StateName;

private:
	ESeedState SeedState;

public:
	ESeedState GetSeedState() { return SeedState; };
	void NextState();
	




};
