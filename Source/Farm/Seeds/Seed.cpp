// Fill out your copyright notice in the Description page of Project Settings.

#include "Seed.h"

Seed::Seed()
{
	SeedState = ESeedState::Initial;
	StateName = "Initial";
}

Seed::~Seed()
{
}

void Seed::NextState()
{
	switch (SeedState)
	{
	case ESeedState::Initial :
		SeedState = ESeedState::Intermediate;
		StateName = "Intermediate";
		break;
	case ESeedState::Intermediate:
		SeedState = ESeedState::Final;
		StateName = "Final";
		break;
	case ESeedState::Final:
		SeedState = ESeedState::Rotten;
		StateName = "Final";
		break;
	}

}
