// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FSeed.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStateChanged, FString, SeedState, int32, Index);

UENUM(BlueprintType)
enum class ESeedState :uint8
{	
	Initial			UMETA(DisplayName = "Initial"),
	Intermediate	UMETA(DisplayName = "Intermediate"),
	Final			UMETA(DisplayName = "Final"),
	Rotten			UMETA(DisplayName = "Rotten")
};

//enum ESeedState {Initial, Intermediate, Final, Rotten};


UCLASS()
class FARM_API AFSeed : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFSeed();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UPROPERTY()
	FOnStateChanged StateChanged;

	int32 SeedIndex;

	UPROPERTY(VisibleAnywhere,BlueprintReadWrite,Category = "Components")
	UStaticMeshComponent* SeedSM;

	float ActualTime;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "SeedState")
	float TimetoChange;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "SeedName")
	FString SeedName;
	FString StateName;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "SeedState")
	ESeedState SeedState;

	uint32 ID;

private:

public:
	ESeedState GetSeedState() { return SeedState; };
	void NextState();
	
	UFUNCTION(BlueprintImplementableEvent,Category ="Farm")
	void OnStateChanged(ESeedState _currentSeedState);


	
};
