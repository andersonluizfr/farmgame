// Fill out your copyright notice in the Description page of Project Settings.

#include "FSeed.h"


// Sets default values
AFSeed::AFSeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SeedSM = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Seed StaticMesh"));
	RootComponent = SeedSM;

	SeedState = ESeedState::Initial;
	StateName = "Initial";
}

// Called when the game starts or when spawned
void AFSeed::BeginPlay()
{
	Super::BeginPlay();
	
}



void AFSeed::NextState()
{
	switch (SeedState)
	{
	case ESeedState::Initial:
		SeedState = ESeedState::Intermediate;
		StateName = "Intermediate";
		OnStateChanged(SeedState);
		StateChanged.Broadcast(StateName, SeedIndex);
		break;
	case ESeedState::Intermediate:
		SeedState = ESeedState::Final;
		StateName = "Final";
		OnStateChanged(SeedState);
		StateChanged.Broadcast(StateName, SeedIndex);
		break;
	case ESeedState::Final:
		SeedState = ESeedState::Rotten;
		StateName = "Final";
		OnStateChanged(SeedState);
		StateChanged.Broadcast(StateName, SeedIndex);
		break;
	}


}






