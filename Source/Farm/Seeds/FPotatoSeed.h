// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FSeed.h"
#include "FPotatoSeed.generated.h"

UCLASS()
class FARM_API AFPotatoSeed : public AFSeed
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFPotatoSeed();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	
	
};
