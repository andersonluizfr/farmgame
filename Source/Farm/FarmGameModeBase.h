// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FarmGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class FARM_API AFarmGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	AFarmGameModeBase();
	
	
	
};
